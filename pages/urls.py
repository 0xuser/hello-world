from django.urls import path
from .views import homePageView

# Create Path
urlpatterns = [
    path('', homePageView, name='home')
]